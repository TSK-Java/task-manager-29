package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

}
