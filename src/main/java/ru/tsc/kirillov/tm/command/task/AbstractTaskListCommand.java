package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskListCommand extends AbstractTaskCommand {

    protected void printTask(@NotNull final List<Task> tasks) {
        int idx = 0;
        for(final Task task: tasks) {
            if (task == null)
                continue;
            System.out.println(++idx + ". " + task);
        }
    }

}
