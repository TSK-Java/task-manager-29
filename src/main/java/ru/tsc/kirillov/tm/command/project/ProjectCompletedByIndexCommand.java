package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.NumberUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectCompletedByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-completed-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Завершить проект по его индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Завершение проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        getProjectService().changeProjectStatusByIndex(getUserId(), NumberUtil.fixIndex(index), Status.COMPLETED);
    }

}
