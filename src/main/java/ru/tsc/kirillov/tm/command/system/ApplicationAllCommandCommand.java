package ru.tsc.kirillov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.model.ICommand;
import ru.tsc.kirillov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationAllCommandCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "commands";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение списка команд.";
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final ICommand cmd: commands)
            System.out.println(cmd.getName());
    }

}
